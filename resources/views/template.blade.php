<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>
        <!-- Bootstrap -->
        <link name="bootstrap" rel="stylesheet" href="{{ asset('css/boot.min.css') }}">
        <!-- MetisMenu -->
        <link name="metismenu" rel="stylesheet" href="{{ asset('css/metisMenu.min.css') }}">
        <!-- SB Admin 2 -->
        <link name="sbadmin" rel="stylesheet" href="{{ asset('css/sb-admin-2.css') }}">
        <!-- Font-awesome -->
        <link name="fontawesome" rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
        <!-- DataTables-->
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
        <!-- Custom -->
        <link name="custom" rel="stylesheet" href="{{ asset('css/custom.css') }}">
        <style>
        @if(Auth::guest()) 
            #page-wrapper {
                margin-left: 250;
            }
        @endif
        </style>
        @yield('head')
    </head>
<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('home') }}">Music Collection App</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> {{Auth::user()->name}} <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        </li>
                        <li>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out fa-fw"></i>
                                Sair
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
            @include('sidebar', [])
        </nav>

        <div id="page-wrapper">
            <br/>
            <br/>
            <br/>
            @yield('content')
        </div>
        <!-- /#page-wrapper -->
    </div>


    <!-- JQuery -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('js/boot.min.js') }}"></script>
    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('js/metisMenu.min.js') }}"></script>
    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('js/sb-admin-2.js') }}"></script>
    <!--Jquery Mask -->
    <script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
    <!--DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <!-- delete -->
    <script>
        $(".btn-delete").on("click", function(e){
            return confirm("You want delete this?");
        })
    </script>
    @yield('scripts')
</body>

</html>
