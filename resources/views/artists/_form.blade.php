<div class="col col-md-12">
  <div class="form-group">
    {!! Form::label('artist_name','Artist name:') !!}
    {!! Form::text('artist_name', null, ['class'=>'form-control']) !!}
  </div>
</div>

<div class="col col-md-12">
  <div class="form-group">
    {!! Form::label('twitter_handle','Twitter handle:') !!}
    {!! Form::text('twitter_handle', null, ['class'=>'form-control']) !!}
  </div>
</div>
