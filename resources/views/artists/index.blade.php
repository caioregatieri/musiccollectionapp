@extends('template')

@section('title')
Artists
@endsection

@section('content')

    <div class="panel panel-default">
      <!-- Default panel contents -->
      <div class="panel-heading clearfix">
        <div class="btn-group pull-left">
          <a href="{{ route('artists.create')}}" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> New</a>
        </div>
        <h4 class="panel-title pull-right" style="padding-top: 7.5px;"></h4>
      </div>
      <!-- Table -->
      <table class="table table-striped table-responsible table-sm" id="table-artists" style="padding: 10px;">
          <thead>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Twitter handle</th>
              <th>Actions</th>
            </tr>
          <thead>
      </table>
    </div>

@endsection

@section('scripts')
<script>
    $(function() {
        $('#table-artists').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('artists.datatables') !!}',
            columns: [
                { data: 'id',             name: 'id' },
                { data: 'artist_name',    name: 'artist_name' },
                { data: 'twitter_handle', name: 'twitter_handle' },
                { data: 'action',         name: 'action', orderable: false, searchable: false }
            ],
            language: {
                sEmptyTable: "Data table is empty",
                sInfo: "Show from _START_ to _END_ of _TOTAL_ rows",
                sInfoEmpty: "Show from 0 to 0 to 0 rows",
                sInfoFiltered: "(Filter of _MAX_ rows)",
                sInfoPostFix: "",
                sInfoThousands: ".",
                sLengthMenu: "_MENU_ rows per page",
                sLoadingRecords: "Loading...",
                sProcessing: "Processing...",
                sZeroRecords: "Nothing to show",
                sSearch: "Search",
                oPaginate: {
                    sNext: "Next",
                    sPrevious: "Previous",
                    sFirst: "First",
                    sLast: "Last"
                },
                oAria: {
                    sSortAscending: ": Order to ASC",
                    sSortDescending: ": Order to DESC"
                }
            }
        });

        $('document').on('click', '.btn-delete',function(e){
            e.preventDefault();
            return confirm('You want delete this?');
        })
    });
</script>
@endsection
