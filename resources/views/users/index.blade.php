@extends('template')

@section('title')
Users
@endsection

@section('content')

    <div class="panel panel-default">
      <!-- Default panel contents -->
      <div class="panel-heading clearfix">
        <div class="btn-group pull-left">
          <a href="{{ route('users.create')}}" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> New</a>
        </div>
        <h4 class="panel-title pull-right" style="padding-top: 7.5px;"></h4>
      </div>
      <!-- Table -->
      <table class="table table-striped table-responsible table-sm" id="table-users" style="padding: 10px;">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>E-mail</th>
            <th>Username</th>
            <th></th>
          </tr>
        </thead>
      </table>
    </div>

@endsection

@section('scripts')
<script>
    $(function() {
        $('#table-users').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('users.datatables') !!}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'email', name: 'email' },
                { data: 'username', name: 'username' },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ],
            language: {
                sEmptyTable: "Data table is empty",
                sInfo: "Show from _START_ to _END_ of _TOTAL_ rows",
                sInfoEmpty: "Show from 0 to 0 to 0 rows",
                sInfoFiltered: "(Filter of _MAX_ rows)",
                sInfoPostFix: "",
                sInfoThousands: ".",
                sLengthMenu: "_MENU_ rows per page",
                sLoadingRecords: "Loading...",
                sProcessing: "Processing...",
                sZeroRecords: "Nothing to show",
                sSearch: "Search",
                oPaginate: {
                    sNext: "Next",
                    sPrevious: "Previous",
                    sFirst: "First",
                    sLast: "Last"
                },
                oAria: {
                    sSortAscending: ": Order to ASC",
                    sSortDescending: ": Order to DESC"
                }
            }
        });

        $('.table').on('click', '.btn-delete', function(e){
            e.preventDefault();
            return confirm('You want delete this?');
        })
    });
</script>
@endsection