-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.1.30-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para musiccollectionapp
DROP DATABASE IF EXISTS `musiccollectionapp`;
CREATE DATABASE IF NOT EXISTS `musiccollectionapp` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `musiccollectionapp`;

-- Copiando estrutura para tabela musiccollectionapp.albums
DROP TABLE IF EXISTS `albums`;
CREATE TABLE IF NOT EXISTS `albums` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `artist_id` int(10) unsigned NOT NULL,
  `album_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `albums_artist_id_foreign` (`artist_id`),
  CONSTRAINT `albums_artist_id_foreign` FOREIGN KEY (`artist_id`) REFERENCES `artists` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Copiando dados para a tabela musiccollectionapp.albums: ~22 rows (aproximadamente)
DELETE FROM `albums`;
/*!40000 ALTER TABLE `albums` DISABLE KEYS */;
INSERT INTO `albums` (`id`, `artist_id`, `album_name`, `year`, `created_at`, `updated_at`) VALUES
	(3, 2, 'Everybody', 2017, '2018-10-12 23:18:46', '2018-10-12 23:18:46'),
	(4, 2, 'Under pressure', 2014, '2018-10-12 23:19:10', '2018-10-12 23:19:10'),
	(5, 2, 'The incredible true story', 2015, '2018-10-12 23:19:57', '2018-10-12 23:19:57'),
	(6, 3, 'Love yourself: Tear', 2018, '2018-10-13 00:40:32', '2018-10-13 00:40:32'),
	(7, 3, 'Face yourself', 2018, '2018-10-13 00:40:58', '2018-10-13 00:40:58'),
	(8, 4, 'Scorpion', 2018, '2018-10-13 00:41:21', '2018-10-13 00:41:21'),
	(9, 4, 'More life', 2017, '2018-10-13 00:41:37', '2018-10-13 00:41:37'),
	(10, 5, 'Kamikaze', 2018, '2018-10-13 00:42:07', '2018-10-13 00:42:07'),
	(11, 5, 'Revival', 2017, '2018-10-13 00:42:28', '2018-10-13 00:42:28'),
	(13, 6, 'Envolve', 2017, '2018-10-13 00:43:09', '2018-10-13 00:43:09'),
	(14, 6, 'Smoke + mirrors', 2015, '2018-10-13 00:43:33', '2018-10-13 00:43:33'),
	(15, 6, 'Night visions', 2012, '2018-10-13 00:43:55', '2018-10-13 00:43:55'),
	(16, 7, 'Beerbongs & Bentleys', 2018, '2018-10-13 00:44:26', '2018-10-13 00:44:26'),
	(17, 7, 'Ultra', 2017, '2018-10-13 00:44:42', '2018-10-13 00:44:42'),
	(18, 8, '17', 2017, '2018-10-13 00:45:13', '2018-10-13 00:45:13'),
	(19, 8, '21XXX', 2017, '2018-10-13 00:45:26', '2018-10-13 00:45:26'),
	(20, 8, 'Revenge', 2017, '2018-10-13 00:45:42', '2018-10-13 00:45:42'),
	(21, 9, 'Sweetener', 2018, '2018-10-13 00:46:06', '2018-10-13 00:46:06'),
	(22, 9, 'Dangerous Woman', 2016, '2018-10-13 00:46:24', '2018-10-13 00:46:32'),
	(23, 10, 'Tha Carter V', 2018, '2018-10-13 00:47:08', '2018-10-13 00:47:08'),
	(24, 10, 'Tha Carter |||', 2008, '2018-10-13 00:47:29', '2018-10-13 00:47:29'),
	(26, 10, 'Teste', 2018, '2018-10-13 01:26:31', '2018-10-13 01:26:31');
/*!40000 ALTER TABLE `albums` ENABLE KEYS */;

-- Copiando estrutura para tabela musiccollectionapp.artists
DROP TABLE IF EXISTS `artists`;
CREATE TABLE IF NOT EXISTS `artists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `artist_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter_handle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Copiando dados para a tabela musiccollectionapp.artists: ~9 rows (aproximadamente)
DELETE FROM `artists`;
/*!40000 ALTER TABLE `artists` DISABLE KEYS */;
INSERT INTO `artists` (`id`, `artist_name`, `twitter_handle`, `created_at`, `updated_at`) VALUES
	(2, 'Logic', '@Logic301', '2018-10-12 22:59:46', '2018-10-13 01:57:13'),
	(3, 'BTS', '@BTS_twt', '2018-10-12 23:00:47', '2018-10-13 01:56:31'),
	(4, 'Drake', '@Drake', '2018-10-12 23:01:31', '2018-10-13 01:56:09'),
	(5, 'Eminem', '@Eminem', '2018-10-12 23:02:07', '2018-10-13 01:57:22'),
	(6, 'Imagine Dragons', '@Imaginedragons', '2018-10-12 23:02:41', '2018-10-13 01:57:41'),
	(7, 'Post Malone', '@PostMalone', '2018-10-12 23:04:01', '2018-10-13 01:57:59'),
	(8, 'XXXtentacion', '@xxxtentacion', '2018-10-12 23:04:45', '2018-10-13 01:58:15'),
	(9, 'Ariana Grande', '@ArianaGrande', '2018-10-12 23:05:13', '2018-10-13 01:58:32'),
	(10, 'Lil Wayne', '@LilTunechi', '2018-10-12 23:06:03', '2018-10-13 01:58:52');
/*!40000 ALTER TABLE `artists` ENABLE KEYS */;

-- Copiando estrutura para tabela musiccollectionapp.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Copiando dados para a tabela musiccollectionapp.migrations: ~4 rows (aproximadamente)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2018_07_02_184858_create_artist_table', 1),
	(4, '2018_07_02_184917_create_album_table', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Copiando estrutura para tabela musiccollectionapp.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Copiando dados para a tabela musiccollectionapp.password_resets: ~0 rows (aproximadamente)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Copiando estrutura para tabela musiccollectionapp.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Copiando dados para a tabela musiccollectionapp.users: ~1 rows (aproximadamente)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Administrador', 'admin@admin.com', 'admin', '$2y$10$Mc8GDcxMhqhrtl3m9qbOeeky7Drq6ulM/Ns2aPSGf5ftcTOIbMqVK', 'M505QV8tpaCn0ukVrysYDHdY5NmRVRtPmyuB61PGORi69vv0VfC2WNh60jPt', '2018-10-12 22:55:28', '2018-10-12 22:55:28');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
