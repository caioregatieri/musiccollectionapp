<div class="col col-md-12">
  <div class="form-group">
    {!! Form::label('artist_id','Artist:') !!}
    {!! Form::select('artist_id', $artists, null, ['class'=>'form-control']) !!}
  </div>
</div>

<div class="col col-md-12">
  <div class="form-group">
    {!! Form::label('album_name','Album name:') !!}
    {!! Form::text('album_name', null, ['class'=>'form-control']) !!}
  </div>
</div>

<div class="col col-md-12">
  <div class="form-group">
    {!! Form::label('year','Year:') !!}
    {!! Form::text('year', null, ['class'=>'form-control']) !!}
  </div>
</div>
