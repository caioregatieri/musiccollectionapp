<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

Use App\Artist;
use Yajra\Datatables\Datatables;

class ArtistController extends Controller
{
    public function index() {
        return view('artists.index');
    }

    public function create() {
        return view('artists.create');
    }

    public function store(Request $request) {
        $this->valida($request);
        Artist::create($request->all());
        return redirect()->route('artists.index');
    }

    public function edit($id) {
        $artist = Artist::find($id);
        return view('artists.edit', compact('artist'));
    }

    public function update(Request $request, $id) {
        $this->valida($request);
        Artist::find($id)->update($request->all());
        return redirect()->route('artists.index');
    }

    public function destroy($id) {
        Artist::destroy($id);
        return redirect()->route('artists.index');
    }

    public function datatables() {
        return Datatables::of(Artist::query())
        ->addColumn('action', function ($artist) {
            $routeEdit = route('artists.edit', ['id'=> $artist->id]);
            $routeDelete = route('artists.destroy', ['id'=> $artist->id]);
            return "<a href='" .$routeEdit. "' class='btn btn-primary btn-sm'>" . 
                        "<i class='fa fa-pencil'></i> Edit" . 
                    "</a>" . 
                    "&nbsp;" .
                    "<a href='" .$routeDelete. "' class='btn btn-danger btn-sm btn-delete'>" . 
                        "<i class='fa fa-trash'></i> Delete" . 
                    "</a>";
        })
        ->make(true);
    }

    public function valida($request){
        $request->validate([
            'artist_name'    => 'required|max:100',
            'twitter_handle' => 'required|max:100'
        ],[
            'artist_name.required' => 'The artist name field is required',
            'artist_name.max' => 'The artist name max length is 100 chars',
            'twitter_handle.required' => 'The twitter handle field is required',
            'twitter_handle.max' => 'The twitter handle max length is 100 chars',
        ]);
    }
}
