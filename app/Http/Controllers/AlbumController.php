<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

Use App\Artist;
Use App\Album;

use Yajra\Datatables\Datatables;

class AlbumController extends Controller
{
    public function index() {
        return view('albums.index');
    }

    public function create() {
        $artists = Artist::all()->pluck('artist_name', 'id');
        return view('albums.create', compact('artists'));
    }

    public function store(Request $request) {
        $this->valida($request);
        Album::create($request->all());
        return redirect()->route('albums.index');
    }

    public function edit($id) {
        $artists = Artist::all()->pluck('artist_name', 'id');
        $album = Album::find($id);
        return view('albums.edit', compact('album','artists'));
    }

    public function update(Request $request, $id) {
        $this->valida($request);
        Album::find($id)->update($request->all());
        return redirect()->route('albums.index');
    }

    public function destroy($id) {
        Album::destroy($id);
        return redirect()->route('albums.index');
    }
    
    public function datatables() {
        $model = Album::with(['artist']);
        return Datatables::of($model)
        ->editColumn('artist_name', function(Album $album) {
            return $album->artist->artist_name;
        })
        ->addColumn('action', function ($album) {
            $routeEdit = route('albums.edit', ['id'=> $album->id]);
            $routeDelete = route('albums.destroy', ['id'=> $album->id]);
            return "<a href='" .$routeEdit. "' class='btn btn-primary btn-sm'>" . 
                        "<i class='fa fa-pencil'></i> Edit" . 
                    "</a>" . 
                    "&nbsp;" .
                    "<a href='" .$routeDelete. "' class='btn btn-danger btn-sm btn-delete'>" . 
                        "<i class='fa fa-trash'></i> Delete" . 
                    "</a>";
        })
        ->make(true);
    }

    public function valida($request){
        $request->validate([
            'artist_id'  => 'required|integer',
            'album_name' => 'required|max:100',
            'year'       => 'required|integer',
        ],[
            'artist_id.required'  => 'Select a artist',
            'album_name.required' => 'The album name field is required',
            'year.required'       => 'The year field is required',
            'artist_id.integer'   => 'The artist id field need be a number',
            'album_name.max'      => 'The alnum name max length is 100 chars',
            'year.integer'        => 'The year field need be a number',
        ]);
    }
}
