@extends('template')

@section('title')
Artist
@endsection

@section('content')

@if($errors->any())
  <div class="alert alert-danger" role="alert">
    <strong>Whoops!</strong> There are errors.<br><br>
    <ul>
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Editing</h3>
  </div>
  <div class="panel-body">

    {!! Form::model($artist, ['route'=>['artists.update', $artist->id], 'method'=>'post']) !!}

      @include('artists._form')

      <div class="col col-md-12">
        <a href="{{ route('artists.index') }}" class="btn btn-default"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</a>
        <button type="submit" class="btn btn-success btn-save"><i class="fa fa-check"></i> Save</button>
      </div>

    {!! Form::close() !!}
  </div>
</div>

@endsection
