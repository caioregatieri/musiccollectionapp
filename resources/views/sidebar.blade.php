<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="{{ route('users.index') }}"><i class="fa fa-user fa-fw"></i> Users</a>
            </li>
            <li>
                <a href="{{ route('artists.index') }}"><i class="fa fa-user-md fa-fw"></i> Artists</a>
            </li>
            <li>
                <a href="{{ route('albums.index') }}"><i class="fa fa-users fa-fw"></i> Albums</a>
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->