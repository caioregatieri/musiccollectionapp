<div class="col col-md-12">
  <div class="form-group">
    {!! Form::label('name','Name:') !!}
    {!! Form::text('name', null, ['class'=>'form-control']) !!}
  </div>
</div>

<div class="col col-md-12">
  <div class="form-group">
    {!! Form::label('email','E-mail:') !!}
    {!! Form::text('email', null, ['class'=>'form-control']) !!}
  </div>
</div>

<div class="col col-md-12">
  <div class="form-group">
    {!! Form::label('username','Username:') !!}
    {!! Form::text('username', null, ['class'=>'form-control']) !!}
  </div>
</div>

<div class="col col-md-12">
  <div class="form-group">
    {!! Form::label('password','Password:') !!}
    {!! Form::password('password', ['class'=>'form-control']) !!}
  </div>
</div>

<div class="col col-md-12">
  <div class="form-group">
    {!! Form::label('password_confirmation','Password confirmation:') !!}
    {!! Form::password('password_confirmation', ['class'=>'form-control']) !!}
  </div>
</div>
