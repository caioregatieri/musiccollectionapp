<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});

Route::get('home', ['as'=>'home','uses'=>'HomeController@index']);

Auth::routes();

Route::group(['middleware' => 'auth'], function(){
  
    Route::group(['prefix'=>'users'], function(){
      Route::get('',             ['as'=>'users.index','uses'=>'UserController@index']);
      Route::get('create',       ['as'=>'users.create', 'uses'=>'UserController@create']);
      Route::post('store',       ['as'=>'users.store', 'uses'=>'UserController@store']);
      Route::get('edit/{id}',    ['as'=>'users.edit', 'uses'=>'UserController@edit']);
      Route::post('update/{id}', ['as'=>'users.update', 'uses'=>'UserController@update']);
      Route::get('destroy/{id}', ['as'=>'users.destroy', 'uses'=>'UserController@destroy']);

      Route::get('datatables',   ['as'=>'users.datatables', 'uses'=>'UserController@datatables']);
    });

    Route::group(['prefix'=>'artists'], function(){
      Route::get('',             ['as'=>'artists.index','uses'=>'ArtistController@index']);
      Route::get('create',       ['as'=>'artists.create', 'uses'=>'ArtistController@create']);
      Route::post('store',       ['as'=>'artists.store', 'uses'=>'ArtistController@store']);
      Route::get('edit/{id}',    ['as'=>'artists.edit', 'uses'=>'ArtistController@edit']);
      Route::post('update/{id}', ['as'=>'artists.update', 'uses'=>'ArtistController@update']);
      Route::get('destroy/{id}', ['as'=>'artists.destroy', 'uses'=>'ArtistController@destroy']);

      Route::get('datatables',   ['as'=>'artists.datatables', 'uses'=>'ArtistController@datatables']);
    });

    Route::group(['prefix'=>'albums'], function(){
      Route::get('',             ['as'=>'albums.index','uses'=>'AlbumController@index']);
      Route::get('create',       ['as'=>'albums.create', 'uses'=>'AlbumController@create']);
      Route::post('store',       ['as'=>'albums.store', 'uses'=>'AlbumController@store']);
      Route::get('edit/{id}',    ['as'=>'albums.edit', 'uses'=>'AlbumController@edit']);
      Route::post('update/{id}', ['as'=>'albums.update', 'uses'=>'AlbumController@update']);
      Route::get('destroy/{id}', ['as'=>'albums.destroy', 'uses'=>'AlbumController@destroy']);

      Route::get('datatables',   ['as'=>'albums.datatables', 'uses'=>'AlbumController@datatables']);
    });

});
